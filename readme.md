# Romatoo Email
It is an emailing platform where people can create their email account, login, sent and receive email with attahments.

## Live website
[www.romatooemail.000webhostapp.com](https://romatooemail.000webhostapp.com/index.php)  

Test account 1: mahathirbishalq@gmail.<span>com, password:1234  
Test account 2: u1@gmail.<span>com,             password:1111


## Deployment/ installation steps
This website is  hosted on [000webhostapp.com](https://www.000webhost.com/)  
steps-  

* First of all, create an account at [www.000webhost.com](https://www.000webhost.com/) and sign in.
* Put a unique website name (ex: myemail) and hit SUBMIT. An empty website is created.
* Go to Manage>Tools>File Manager>Upload Files>public_html, then drag and drop all the files from this repository.
* Again, go to Manage>Tools>Database Manager>New Database and put the database name, Database username and password then hit create (save these info and put them into connection.php file later).
* Next, navigate to Manage databases > phpMyAdmin and select the newly created database.
* Now to create necessary tables hit SQL tab and execute the following queries:

``` sql
CREATE TABLE signup( fname text, lname text,email text, password text);
```
``` sql
CREATE TABLE message( s_email text, r_email text, title text, message text, timee text, datee text, file text, id text, category text);
```


## Requirements Screenshots
##
#### Requirement  R1 (Signup)
![c1](https://gitlab.com/romatoo/romatoo_email/-/raw/main/requirements%20screenshots/signup.PNG)

#### Requirement  R2 (Login)
![c2](https://gitlab.com/romatoo/romatoo_email/-/raw/main/requirements%20screenshots/login.PNG)

#### Requirement  R3.1 (Compose)
![c3](https://gitlab.com/romatoo/romatoo_email/-/raw/main/requirements%20screenshots/compose.PNG)

#### Requirement  R3.2 (sentbox)
![c4](https://gitlab.com/romatoo/romatoo_email/-/raw/main/requirements%20screenshots/sent1.PNG)

#### Requirement  R3.3 (View sent email)
![c5](https://gitlab.com/romatoo/romatoo_email/-/raw/main/requirements%20screenshots/sent2.PNG)

#### Requirement  R4.1 (Inbox)
![c6](https://gitlab.com/romatoo/romatoo_email/-/raw/main/requirements%20screenshots/inbox1.PNG)

#### Requirement  R4.2 (View received email)
![c7](https://gitlab.com/romatoo/romatoo_email/-/raw/main/requirements%20screenshots/inbox2.PNG)

#### Requirement  R5 (Filter email from inbox)
![c7](https://gitlab.com/romatoo/romatoo_email/-/raw/main/requirements%20screenshots/filter__2_.png)

