<?php
session_start();
setcookie("category", "Primary"); //default
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Compose mail</title>
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <div id="header">
        <h1>Romatoo Email</h1>
        <button title="Profile" class="profile_icon" onclick="window.location='profile.php'"> </button>

        <div class="ppp"> <?php echo $_SESSION["user_mail"]; ?></div>
    </div>

    <div id="main_div">
        <div class="sidebar">
            <ul>
                <li><a class="active" href="compose.php">Compose</a></li>
                <li><a href="inbox.php">Inbox</a></li>
                <li><a href="sentbox.php">Sent</a></li>
                <li><a href="profile.php">Profile</a></li>
                <li><a href="logout.php">Logout</a></li>

            </ul>
        </div>

        <div class="main_body">
            <!-- start -->
            <?php
            $nextstage = 1;

            include("connection.php");
            $connection = mysqli_connect($server, $user, $password, $database);



            // 

            if (isset($_POST['submit'])) {
                $s_email = $_POST["from"];
                $r_email = $_POST["to"];
                $title = $_POST["tittle"];
                $message = $_POST["message"];
                $datee = date("Y-m-d");
                $timee = date("h:i:sa", strtotime('+5 hours 30 minutes'));
                
                $now = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
                $id = $now->format("m-d-Y H:i:s.u");  //unique id



                // check valid user
                $connection = mysqli_connect($server, $user, $password, $database);
                $sql = "SELECT * from signup where email='$r_email'";
                $query = mysqli_query($connection, $sql);
                $rownum = mysqli_num_rows($query);


                $selected_cat = $_COOKIE['category'];



                if (!$s_email) {
                    echo '<script>alert("*From required");</script>';
                    $nextstage = 0;
                } elseif (!$r_email) {
                    // echo '<script>alert("* Receiver (To) required");</script>';
                    echo '<script> alert("* Atleast one receiver (To) required"); </script>';
                    $nextstage = 0;
                    echo '<script language="javascript" type="text/javascript"> window.history.back();</script>';
                } elseif ($s_email == $r_email) {
                    echo '<script> alert("* *Sender and Receiver can not be same"); </script>';
                    $nextstage = 0;
                    echo '<script language="javascript" type="text/javascript"> window.history.back();</script>';
                } elseif (!$title) {
                    echo '<script> alert("* A subject required"); </script>';
                    $nextstage = 0;
                    echo '<script language="javascript" type="text/javascript"> window.history.back();</script>';
                } elseif (!$message) {
                    echo '<script> alert("* Email body can not be empty"); </script>';
                    $nextstage = 0;
                    echo '<script language="javascript" type="text/javascript"> window.history.back();</script>';
                } elseif ($rownum == 0) {
                    echo '<script> alert("* Invalid receiver"); </script>';
                    $nextstage = 0;
                    echo '<script language="javascript" type="text/javascript"> window.history.back();</script>';
                } else {


                    //  ------------------------------------------check how many reciever                      
                    $string = $r_email;
                    $string = $string . " ";
                    $spacecount = 0;

                    //multiple receiver at the same time 
                    for ($i = 0; $i < strlen($string); $i = $i + 1) {
                        if ($string[$i] == " ") {
                            $spacecount = $spacecount + 1;
                        }
                    }
                    $email[$spacecount + 1] = array();
                    $k = 0;
                    for ($i = 0; $i <= $spacecount; $i = $i + 1) {
                        $space = strpos($string, " ");
                        $email[$i] = substr($string, 0, $space);
                        $string = substr($string, $space + 1, strlen($string));
                        // echo $email[$i] . "<br>";
                    }
                    //--------------------------all multiple recievers are in email[] array;

                    // checking if there is an attachment
                    // $attachment = 0;
                    // if (isset($_POST["submit"])) {
                    // $attachment = 1;
                // $file = $_FILES["file"]["name"];
                // $tmp_name = $_FILES["file"]["tmp_name"];
                // $path = "upload/" . $file;
                // $file1 = explode(".", $file);
                // $ext = $file1[1];
                // $allowed = array("jpg", "png", "gif", "pdf", "zip", "mp4", "mkv");
                // if (in_array($ext, $allowed)) {
                //     move_uploaded_file($tmp_name, $path);
                // }
                    // }
                    
                    
//--new start
$attachment = false;
 if (!empty($_FILES['file']["name"])) {
        $file = $_FILES["file"]["name"];

        $isValidFile = true;
        $path = "upload/" . $file;
    

        // Validate file extension
        $allowedFileType = array("jpg", "png", "gif", "pdf", "zip", "mp4", "mkv");
        
        // $fileExtension = $file1[1];//strtolower(pathinfo($file, PATHINFO_EXTENSION));
        // if (! in_array($fileExtension, $allowedFileType)) {
        //     echo "<script> alert('File type is not supported11111 ".$fileExtension."');
        //             window.history.back();
        //         </script>";
            
        //     $isValidFile = false;
        // }

        // Validate file size
        if ($_FILES["file"]["size"] > 3*1048576) { //max 3mb
            echo "<script>('File is too large to upload. Max 3MB allowed');
                    window.history.back();
            </script>";
            $isValidFile = 0;
        }

        if ($isValidFile) {
            $attachment = true;
            move_uploaded_file($_FILES["file"]["tmp_name"], $path);
        }
    }


//--new end
                    
                    
                    

                    if ($spacecount == 1) { //-------sending to a single user
                        // if ($nextstage == 1 && $attachment == 1) {
                        if ($attachment) {
                            //  file was selected for upload, your (re)action goes here

                            $sql = "Insert into inbox (s_email,r_email,title,message,datee,timee,id,file,category) values('$s_email','$r_email','$title','$message','$datee','$timee','$id','$file','$selected_cat')";
                            $iquery = mysqli_query($connection, $sql);

                            if ($iquery) {
                                echo "<script>
                                            alert('Mail sent seccessfully with Attachment');
                                            window.location.href='sentbox.php';
                                            </script>";
                            } else {
                                echo 'failed to insert data1' . '<br>';
                            }
                        }
                        // elseif ($attachment == 0) {
                        else {
                            // No file was selected for upload, your (re)action goes here
                            
                            
                            $sql = "Insert into inbox (s_email,r_email,title,message,datee,timee,id,category) values('$s_email','$r_email','$title','$message','$datee','$timee','$id','$selected_cat')";
                            $iquery = mysqli_query($connection, $sql);

                            if ($iquery) {
                                echo "<script>
                                            alert('Mail sent seccessfully ');
                                            window.location.href='sentbox.php';
                                            </script>";
                            } else {
                                echo $selected_cat.'failed to insert data2' . '<br>';
                            }
                        }
                    } else { //-------sending to multiple user                       
                        for ($i = 0; $i < $spacecount; $i = $i + 1) {
                            if (!$_FILES['logo']['name'] == "") {
                                // if ($nextstage == 1 && $attachment == 1) {
                                $sql = "Insert into inbox (s_email,r_email,title,message,datee,timee,id,file,category) values('$s_email','$r_email','$title','$message','$datee','$timee','$id','$file','$selected_cat')";
                                $iquery = mysqli_query($connection, $sql);

                                if ($iquery) {
                                    echo "<script>
                                                alert('Mail sent seccessfully with Attachment');
                                                window.location.href='sentbox.php';
                                                </script>";
                                } else {
                                    echo 'failed to insert data3' . '<br>';
                                }
                            } else {
                                // elseif ($nextstage == 1 && $attachment == 0) {
                                $sql = "Insert into inbox (s_email,r_email,title,message,datee,timee,id,category) values('$s_email','$r_email','$title','$message','$datee','$timee','$id','$selected_cat')";
                                $iquery = mysqli_query($connection, $sql);

                                if ($iquery) {
                                    echo "<script>
                                                alert('Mail sent seccessfully ');
                                                window.location.href='sentbox.php';
                                                </script>";
                                } else {
                                    echo 'failed to insert data4' . '<br>';
                                }
                            }
                        }
                    }
                }
            }

            ?>

            <?php
            if ($nextstage != 0) {


            ?>
                <!-- finish -->
                <form enctype="multipart/form-data" method="post">
                    <input class="from" type="email" name="from" placeholder="From :" value=<?php echo $_SESSION["user_mail"] ?> readonly>

                    <?php if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        $tto = $_POST["to_value"]; ?>
                        <input class="from" type="email" name="to" placeholder="To :" value=<?php echo $tto ?> readonly>
                    <?php } else { ?>
                        <input class="from" type="text" name="to" placeholder="To :">
                    <?php } ?>

                    <input class="from" type="text" name="tittle" placeholder="Subject :">
                    <textarea class="massage" name="message" placeholder="Type your message here .. "></textarea>


                    <script type="text/javascript">
                        function getselected() {
                            var select_container = document.getElementById('selectselected');

                            alert('type: ' + select_container.options[select_container.selectedIndex].value);
                            var selected_cat = select_container.options[select_container.selectedIndex].value;
                            document.cookie = "category=" + selected_cat;
                            <?php
                            echo 'aaaaaaaaaaaaaaaaaa';
                            // $def_cat = false;


                            ?>

                        }
                    </script>



                    
                    <br>
                    <span id="attach"> Attach File : </span>
                    <input class="file_input" type="file" name="file">


                    

                    <select name="category" onchange="getselected();" id="selectselected">
                        <option>Primary</option>
                        <option>Social</option>
                        <option>Promotional</option>
                        <option>Forum</option>
                    </select>

                    <input style="margin-top: 5px;" type="submit" name="submit" value="send">
                    <!-- <button class="draft_button" type="submit" formaction="draft.php">save as draft</button> -->

                    <br>
                    <br>
                </form>

            <?php

            } ?>

        </div>


    </div>
</body>

</html>